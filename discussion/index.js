//[SECTION] Introduction to JSON (JavaScript Object Notation)

//JSON -> is a 'Data Representation Format' similar to XML and YAML

//Uses of JSON data format:

	//=> Commonly used for API and Configuration.
	//=> JSON is also used in serealizing different data types into 'bytes'. 

    //What is Serealizing?
      //=> is the process of converting data into a series of 'bytes' for easier transmission/transfer of information

      //a 'byte' => is a unit of data that is eight binary digits (1 and 0) that is used to represent a character(letters, numbers, typographic symbols). 

    //Benefits => once a piece of data/information has been serialize they become 'lightweight', it becomes a lot easier to transfer or trasmit over a network or connection. 

//[SECTION] Structure of JSON format

  //=> JSON data is similar to the structure of a JS Object.
  //=> JS Objects are NOT to be confused with JSON. 
  //SYNTAX:
    // {
    // 	"propertyA": "valueA",
    // 	"propertyB": "valueB"
    // }

    //JSON uses double quotes ("") for its property names.

    //example: 
    // {
    // 	"city": "Quezon City",
    // 	"province": "Metro Manila",
    // 	"country": "Philippines"
    // }

//[SECTION] JSON types
   //JSON will accept the following values:
   //1. Strings => "Hello", "Hello World"
   //2. Number => 10, 1.5, -30, 1.2e10
   //3. Boolean => True or False
   //4. null => empty/null
   //5. Array => []
   //6. Object => { key: value }

  //NOTE: JSON files is also commonly used to store a series of information. 

       //create a file called employees.json (store multiple information about a company's employees)

  //KEEP THIS IN MIND:
    //anything that you write in JSON, is VALID JAVASCRIPT. the syntaxes will be recognized properly by JS. 
let employees = [
  {
    "name": "Thonie Fernandez",
    "department": "Instructor",
    "yearEmployed": "2020",
    "ratings": 5.0 
  },
  {
    "name": "Charles Quimpo",
    "department": "Instructor",
    "yearEmployed": "2010",
    "ratings": 5.0 
  },
  {
    "name": "Martin Miguel",
    "department": "Instructor",
    "yearEmployed": "2019",
    "ratings": 4.0 
  },
  {
    "name": "Alvin Estiva",
    "department": "Instructor",
    "yearEmployed": "2020",
    "ratings": 5.0 
  }
]; 

let user = {
   "name": "Kyle",
   "favoriteNumber": 7,
   "isProgrammer": true,
   "hobbies": [ 
     "Weight Lifting",
     "Reading Comics",
     "Playing the Guitar"
   ],
   "friends": [
     {
      "name": "Robin",
      "isProgrammer": true
     },
     {
      "name": "Daniel",
      "isProgrammer": false
     },
     {
      "name": "Oswald",
      "isProgrammer": true
     }
   ]
}

  // console.log(user); 

  //NOTE: Another use case of a JSON file, JSON file is commonly used to describe an application/project. 

  //if you are going to create a node project it will usually come with package.json file.

  //"package.json" -> is considered as the "heart" of any application, used to describe the overall structure of an application. 
let application = `{
  "name": "javascript server", 
  "version": "1.0",
  "description": "server side application done using javascript and node js",
  "main": "index.js",
  "scripts": {
    "start": "node index.js"
  }, 
  "keywords": [
    "server",
    "node",
    "backend"
  ],
  "author": "John Smith",
  "license": "ISC"  
}`; 

  console.log(application);
  console.log(typeof application); //data type -> object => all strings

  //When dealing with JSON, you will usually retrieve this data format in all strings, in order for us to emulate that we will use back ticks. 

  //Whenever your dealing with JSON data that are in all string, the data you received is NOT very usable. 
  console.log(application.name); 

//[SECTION] Different JSON Methods

    //-> The HSON object contains method for parsing and converting data into 'stringified' JSON.

//1. HOW TO CONVERT A JSON data to the 'STRINGIFIED VERSION'. 
console.log(employees); 
   // JSON.stringify() -> will allow us to convert a JSON object into all strings.
   let jsonString = JSON.stringify(employees); 
   console.log(jsonString); 
   //WHAT IS THE USE CASE: this is commonly used when sending HTTP requests, where information is required to be sent and received in a "stringified" JSON format. 

//2. HOW TO CONVERT JSON Strings into JSON Objects
    
    // JSON.parse() -> will allow us to convert the stringified object into a JS object. 

   let jsonObject = JSON.parse(jsonString);
   console.log(jsonObject);
   //upon converting a stringed JSON to js object, you will now be able to use and access the data stored.
   console.log(jsonObject[0].name);  
   console.log(JSON.parse(application).description); 
